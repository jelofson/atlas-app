<?php
// include after instantiating the container in the bootstrap file
use Vespula\Log\Log as Logger;
use Vespula\Log\Adapter\ErrorLog;
use Vespula\Auth\Session\Session as AuthSession;
use Vespula\Auth\Auth;
use Vespula\Auth\Adapter\Text as AuthTextAdapter;
use Blog\DataSource\Authors\AuthorsMapper;
use Blog\DataSource\Posts\PostsMapper;
use Blog\DataSource\Tags\TagsMapper;
use Blog\DataSource\Taggings\TaggingsMapper;

// Set the theme
$container['theme'] = 'bootstrap';


$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $view = $container->get('view');
        $locale = $container->get('locale');
        $content = $view->render('error', [
            'theme'=>$container->theme,
            'message'=>$locale->gettext('ERROR_404'),
            'messages'=>[]
        ]);
        return $response
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write($content);
    };
};

$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        $view = $container->get('view');
        $content = $view->render('error', [
            'theme'=>$container->theme,
            'message'=>'500 Server Error',
            'details'=>$exception,
            'messages'=>[]
        ]);
        return $response
            ->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($content);
    };
};

$container['phpErrorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        $view = $container->get('view');
        $content = $view->render('error', [
            'theme'=>$container->theme,
            'message'=>'500 Server Error',
            'details'=>$exception,
            'messages'=>[]
        ]);
        return $response
            ->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($content);
    };
};

$container['view'] = function ($container) {

    $dir = dirname(__FILE__);
    $view = new League\Plates\Engine($dir . '/Views');
    $view->addFolder('layouts', $dir . '/Layouts');
    $uri = $container->get('request')->getUri();
    $view->addData([
        'baseUri'=>$uri->getBasePath(),
        'title'=>'Fun with Atlas',
        'theme'=>$container->theme,
        'locale'=>$container->get('locale'),
        'auth'=>$container->get('auth'),
        'router'=>$container->get('router')
    ]);
    $alerts = new \Blog\Views\Extensions\Alerts($container);
    $alerts->setTheme($container->theme);
    $view->loadExtension($alerts);
    return $view;
};

$container['session'] = function () {
	$sessionFactory = new \Aura\Session\SessionFactory();
	return $sessionFactory->newInstance($_COOKIE);
};

$container['locale'] = function () {
    $locale = new \Vespula\Locale\Locale('en_CA');
    $locale->load(__DIR__ . '/Locales');
    return $locale;
};

// Create a logger. See the Vespula.Log documentation for the different
// adapters and log types.
$container['log'] = function () {
    // Log to PHP's default logger (if error_log set in ini)
    $adapter = new ErrorLog(ErrorLog::TYPE_PHP_LOG);
    $log = new Logger($adapter);
    return $log;
};

$container['auth'] = function () {
    $session = new AuthSession();
    $passwords = [
        'juser'=>'$2y$10$6iQyfII574FqLgOdoKsUl.ubMvqpu9EGigN/4b4r/9X6GVBRrUoxO'
    ];
    $adapter = new AuthTextAdapter($passwords);

    $adapter->setUserdata('juser', [
        'fullname'=>'Joe User',
        'email'=>'juser@example.com'
    ]);
    $auth = new Auth($adapter, $session);
    return $auth;
};

$container['eventHandler'] = function () {
    return new \Vespula\Event\EventHandler();
};

$container['atlas'] = function () {
    $conn = include __DIR__ . '/conn.php';
    $atlasContainer = new \Atlas\Orm\AtlasContainer($conn[0]);
    $atlasContainer->setMappers([
        AuthorsMapper::CLASS,
        TagsMapper::CLASS,
        PostsMapper::CLASS,
        TaggingsMapper::CLASS,
    ]);

    return $atlasContainer->getAtlas();
};

$container['form'] = function () {
    $form = new \Vespula\Form\Form;
    return $form;
};
