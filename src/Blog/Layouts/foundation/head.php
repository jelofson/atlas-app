    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $this->e($title); ?></title>
        <link rel="stylesheet" href="<?=$baseUri; ?>/foundation/css/foundation.css" />
        <link rel="stylesheet" href="<?=$baseUri; ?>/foundation/css/app.css" />
    </head>