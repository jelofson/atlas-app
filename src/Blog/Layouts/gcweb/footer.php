        <footer role="contentinfo" id="wb-info">
            <nav role="navigation" class="container visible-sm visible-md visible-lg wb-navcurr">
                <h2 class="wb-inv"><?php echo $locale->gettext('TEXT_ABOUT_SITE'); ?></h2>
                <div class="row">
                    <div class="col-sm-3 col-lg-3">
                        <section>
                            <h3><?php echo $locale->gettext('TEXT_CONTACT_INFORMATION'); ?></h3>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo $locale->gettext('URL_MOST_TOPICS'); ?>"><?php echo $locale->gettext('TEXT_MOST_TOPICS'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_SPECIALIZED_ENQUIRIES'); ?>"><?php echo $locale->gettext('TEXT_SPECIALIZED_ENQUIRIES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_GENERAL_ENQUIRIES'); ?>"><?php echo $locale->gettext('TEXT_GENERAL_ENQUIRIES'); ?></a></li>
                            </ul>
                        </section>
                        <section>
                            <h3>News</h3>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo $locale->gettext('URL_NEWSROOM'); ?>"><?php echo $locale->gettext('TEXT_NEWSROOM'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_NEWS_RELEASES'); ?>"><?php echo $locale->gettext('TEXT_NEWS_RELEASES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_MEDIA_ADVISORIES'); ?>"><?php echo $locale->gettext('TEXT_MEDIA_ADVISORIES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_SPEECHES'); ?>"><?php echo $locale->gettext('TEXT_SPEECHES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_STATEMENTS'); ?>"><?php echo $locale->gettext('TEXT_STATEMENTS'); ?></a></li>
                            </ul>
                        </section>
                    </div>
                    <section class="col-sm-3 col-lg-3">
                        <h3>Government</h3>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo $locale->gettext('URL_HOW_GOV_WORKS'); ?>"><?php echo $locale->gettext('TEXT_HOW_GOV_WORKS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_DEPTS_AGENCIES'); ?>"><?php echo $locale->gettext('TEXT_DEPTS_AGENCIES'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_PRIME_MINISTER'); ?>"><?php echo $locale->gettext('TEXT_PRIME_MINISTER'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_MINISTERS'); ?>"><?php echo $locale->gettext('TEXT_MINISTERS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_PUBSERVICE_MILITARY'); ?>"><?php echo $locale->gettext('TEXT_PUBSERVICE_MILITARY'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_POLICIES_REGS'); ?>"><?php echo $locale->gettext('TEXT_POLICIES_REGS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_LIBRARIES'); ?>"><?php echo $locale->gettext('TEXT_LIBRARIES'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_PUBLICATIONS'); ?>"><?php echo $locale->gettext('TEXT_PUBLICATIONS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_STATISTICS_DATA'); ?>"><?php echo $locale->gettext('TEXT_STATISTICS_DATA'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_ABOUT_CANADA_CA'); ?>"><?php echo $locale->gettext('TEXT_ABOUT_CANADA_CA'); ?></a></li>
                        </ul>
                    </section>
                    <section class="col-sm-3 col-lg-3 brdr-lft">
                        <h3><?php echo $locale->gettext('TEXT_TRANSPARENCY'); ?></h3>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo $locale->gettext('URL_GOV_WIDE_REPORTING'); ?>"><?php echo $locale->gettext('TEXT_GOV_WIDE_REPORTING'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_OPEN_GOVERNMENT'); ?>"><?php echo $locale->gettext('TEXT_OPEN_GOVERNMENT'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_PRODIS'); ?>"><?php echo $locale->gettext('TEXT_PRODIS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_TERMS_CONDITIONS'); ?>"><?php echo $locale->gettext('TEXT_TERMS_CONDITIONS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_PRIVACY'); ?>"><?php echo $locale->gettext('TEXT_PRIVACY'); ?></a></li>
                        </ul>
                    </section>
                    <div class="col-sm-3 col-lg-3 brdr-lft">
                        <section>
                            <h3><?php echo $locale->gettext('TEXT_FEEDBACK'); ?></h3>
                            <p><a href="<?php echo $locale->gettext('URL_FEEDBACK'); ?>"><img src="<?=$baseUri; ?>/gc/GCWeb/assets/feedback.png" alt="<?php echo $locale->gettext('TEXT_FEEDBACK'); ?>"></a></p>
                        </section>
                        <section>
                            <h3><?php echo $locale->gettext('TEXT_SOCIAL_MEDIA'); ?></h3>
                            <p><a href="<?php echo $locale->gettext('URL_SOCIAL_MEDIA'); ?>"><img src="<?=$baseUri; ?>/gc/GCWeb/assets/social.png" alt="<?php echo $locale->gettext('TEXT_SOCIAL_MEDIA'); ?>"></a></p>
                        </section>
                        <section>
                            <h3><?php echo $locale->gettext('TEXT_MOBILE_CENTRE'); ?></h3>
                            <p><a href="<?php echo $locale->gettext('URL_MOBILE_CENTRE'); ?>"><img src="<?=$baseUri; ?>/gc/GCWeb/assets/mobile.png" alt="<?php echo $locale->gettext('TEXT_MOBILE_CENTRE'); ?>"></a></p>
                        </section>
                    </div>
                </div>
            </nav>
            <div class="brand">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 visible-sm visible-xs tofpg">
                        <a href="#wb-cont"><?php echo $locale->gettext('TEXT_TOP_PAGE'); ?> <span class="glyphicon glyphicon-chevron-up"></span></a>
                    </div>
                    <div class="col-xs-6 col-md-12 text-right">
                        <object type="image/svg+xml" tabindex="-1" role="img" data="<?=$baseUri; ?>/gc/GCWeb/assets/wmms-blk.svg" aria-label="<?php echo $locale->gettext('TEXT_WORDMARK'); ?>"></object>
                    </div>
                </div>
                </div>
            </div>
        </footer>
