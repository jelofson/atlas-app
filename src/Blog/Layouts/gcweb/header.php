        <header role="banner">
            <div id="wb-bnr" class="container">
                <section id="wb-lng" class="visible-md visible-lg text-right">
                    <h2 class="wb-inv"><?php echo $locale->gettext('TEXT_LANG_SELECTION'); ?></h2>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-inline margin-bottom-none">
                                <li><a lang="<?php echo $locale->gettext('TEXT_ALT_LANG'); ?>" href="content-fr.html"><?php echo $locale->gettext('TEXT_ALT_LANG_FULL'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <div class="row">
                    <div class="brand col-xs-8 col-sm-9 col-md-6">
                        <a href="<?php echo $locale->gettext('URL_CANADA_CA'); ?>"><object type="image/svg+xml" tabindex="-1" data="<?=$baseUri; ?>/gc/GCWeb/assets/sig-blk-<?php echo $locale->gettext('TEXT_LANG'); ?>.svg"></object><span class="wb-inv"> <?php echo $locale->gettext('TEXT_GOC'); ?></span></a>
                    </div>
                    <section class="wb-mb-links col-xs-4 col-sm-3 visible-sm visible-xs" id="wb-glb-mn">
                        <h2><?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?></h2>
                        <ul class="list-inline text-right chvrn">
                            <li><a href="#mb-pnl" title="<?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?>" aria-controls="mb-pnl" class="overlay-lnk" role="button"><span class="glyphicon glyphicon-search"><span class="glyphicon glyphicon-th-list"><span class="wb-inv"><?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?></span></span></span></a></li>
                        </ul>
                        <div id="mb-pnl"></div>
                    </section>
                    <section id="wb-srch" class="col-xs-6 text-right visible-md visible-lg">
                        <h2 class="wb-inv"><?php echo $locale->gettext('TEXT_SEARCH'); ?></h2>
                        <form action="<?php echo $locale->gettext('URL_SEARCH'); ?>" method="post" name="cse-search-box" role="search" class="form-inline">
                            <div class="form-group">
                                <label for="wb-srch-q" class="wb-inv"><?php echo $locale->gettext('TEXT_SEARCH_WEBSITE'); ?></label>
                                <input id="wb-srch-q" list="wb-srch-q-ac" class="wb-srch-q form-control" name="q" type="search" value="" size="27" maxlength="150" placeholder="<?php echo $locale->gettext('TEXT_SEARCH_PLACEHOLDER'); ?>">
                                <datalist id="wb-srch-q-ac">
                                <!--[if lte IE 9]><select><![endif]-->
                                <!--[if lte IE 9]></select><![endif]-->
                                </datalist>
                            </div>
                            <div class="form-group submit">
                                <button type="submit" id="wb-srch-sub" class="btn btn-primary btn-small" name="wb-srch-sub"><span class="glyphicon-search glyphicon"></span><span class="wb-inv"><?php echo $locale->gettext('TEXT_SEARCH'); ?></span></button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
            <nav role="navigation" id="wb-sm" class="wb-menu visible-md visible-lg" data-trgt="mb-pnl" data-ajax-replace="<?=$baseUri; ?>/gc/ajax/sitemenu-<?php echo $locale->gettext('TEXT_LANG'); ?>.html" typeof="SiteNavigationElement">
                <h2 class="wb-inv"><?php echo $locale->gettext('TEXT_TOPICS_MENU'); ?></h2>
                <div class="container nvbar">
                    <div class="row">
                        <ul class="list-inline menu">
                            <li><a href="<?php echo $locale->gettext('URL_JOBS'); ?>"><?php echo $locale->gettext('TEXT_JOBS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_IMMIGRATION'); ?>"><?php echo $locale->gettext('TEXT_IMMIGRATION'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_TRAVEL'); ?>"><?php echo $locale->gettext('TEXT_TRAVEL'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_BUSINESS'); ?>"><?php echo $locale->gettext('TEXT_BUSINESS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_BENEFITS'); ?>"><?php echo $locale->gettext('TEXT_BENEFITS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_HEALTH'); ?>"><?php echo $locale->gettext('TEXT_HEALTH'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_TAXES'); ?>"><?php echo $locale->gettext('TEXT_TAXES'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_MORE_SERVICES'); ?>"><?php echo $locale->gettext('TEXT_MORE_SERVICES'); ?></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav role="navigation" id="wb-bc" class="" property="breadcrumb">
                <h2 class="wb-inv"><?php echo $locale->gettext('TEXT_YOU_ARE_HERE'); ?></h2>
                <div class="container">
                    <div class="row">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo $locale->gettext('URL_CANADA_CA'); ?>"><?php echo $locale->gettext('TEXT_HOME'); ?></a></li>
                        </ol>
                    </div>
                </div>
            </nav>
        </header>
