<!DOCTYPE html>
<html lang="en">
    
    <?php $this->insert('layouts::bootstrap/head'); ?>
    
    <body> 
        
        <?php $this->insert('layouts::bootstrap/navbar'); ?>
                       
        <div class="container">
            <h1>Bootstrap Layout</h1>
            <?=$this->section('content')?>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?=$baseUri; ?>/bootstrap/js/bootstrap.min.js"></script>
        
        <?=$this->section('scripts'); ?>
        
    </body>
</html>
