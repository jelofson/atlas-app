<?php
// default route
$app->get('/', function ($request, $response) {
    $controller = new \Blog\Controllers\Posts($this, $request, $response);
    return $controller->index();
})->setName('home');

$app->group('/posts/add', function () {
    $this->get('', function ($request, $response) {
        $controller = new \Blog\Controllers\Posts($this, $request, $response);
        return $controller->add();
    })->setName('posts-add');
    $this->post('', function ($request, $response) {
        $controller = new \Blog\Controllers\Posts($this, $request, $response);
        return $controller->doAdd();
    });
});

$app->group('/posts/edit/{id:[0-9]+}', function () {
    $this->get('', function ($request, $response, $args) {
        $controller = new \Blog\Controllers\Posts($this, $request, $response);
        return $controller->edit($args['id']);
    })->setName('posts-edit');
    $this->post('', function ($request, $response, $args) {
        $controller = new \Blog\Controllers\Posts($this, $request, $response);
        return $controller->doEdit($args['id']);
    });
});

$app->get('/posts/{id:[0-9]+}', function ($request, $response, $args) {
    $controller = new \Blog\Controllers\Posts($this, $request, $response);
    return $controller->view($args['id']);
})->setName('posts-view');

$app->post('/authors/add', function ($request, $response) {
    $controller = new \Blog\Controllers\Authors($this, $request, $response);
    return $controller->add();
})->setName('authors-add');


// Define project routes
// These are just example routes
$app->group('/login', function() {
    $this->get('', function ($request, $response) {
        // Replace "Project" with your project namespace
        $controller = new \Blog\Controllers\Index($this, $request, $response);

        return $controller->login();
    });
    $this->post('', function ($request, $response) {
        // Replace "Project" with your project namespace
        $controller = new \Blog\Controllers\Index($this, $request, $response);

        return $controller->doLogin();
    });
});
$app->get('/logout', function ($request, $response) {
    // Replace "Project" with your project namespace
    $controller = new \Blog\Controllers\Index($this, $request, $response);

    return $controller->logout();
});
