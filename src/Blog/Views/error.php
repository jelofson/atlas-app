<?php $this->layout('layouts::' . $theme); ?>

<?=$this->alerts($messages); ?>

<h2><?=$this->e($message); ?></h2>

<?php if (isset($details)) : ?>
<p>
<?php echo $this->e($details); ?>
</p>
<?php endif; ?>
