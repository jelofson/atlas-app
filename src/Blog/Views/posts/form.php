<?php $this->layout('layouts::' . $theme); ?>

<?=$this->alerts($messages); ?>

<?php if ($method == 'add') :?>
    <h2>Add new post</h2>
<?php else : ?>
    <h2>Edit post</h2>
<?php endif; ?>

<?=$form->begin(); ?>
    <div class="form-group">
    <?=$form->label('Title')->for('title'); ?>
    <?=$form->text()->name('title')->id('title')->value($post->title)->class('form-control'); ?>
    </div>
    <div class="form-group">
        <?=$form->label('Body')->for('body'); ?>
        <?=$form->textarea()->name('body')->id('body')->value($post->body)->rows(8)->class('form-control'); ?>
    </div>
    <div class="form-group">
        <?=$form->label('Author')->for('author_id'); ?>
        <?=$form->select()->options($authors)->name('author_id')->id('author_id')->selected($post->author_id)->class('form-control'); ?>
        <?=$form->button('New Author')->class("btn btn-default")->id('btn-author')->data('toggle', 'modal')->data('target', '#new-author'); ?>
    </div>
    <div class="form-group">
        <?=$form->label('Tags')->for('tags'); ?>
        <?=$form->text()->name('tags')->id('tags')->value($post->getTags())->class('form-control'); ?>
    </div>
    <div>
        <?=$form->submit('Save')->class("btn btn-primary"); ?> 
        <a href="<?=$router->pathFor('home'); ?>" class="btn btn-default">Cancel</a>
    </div>
<?= $form->end(); ?>


<div class="modal fade" id="new-author" tabindex="-1" role="dialog" aria-labelledby="New Author Dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Author</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?=$form->label('First Name')->for('first_name'); ?>
                    <?=$form->text()->name('first_name')->id('first_name')->class('form-control'); ?>
                </div>
                <div class="form-group">
                    <?=$form->label('First Name')->for('first_name'); ?>
                    <?=$form->text()->name('last_name')->id('last_name')->class('form-control'); ?>
                </div>
            </div>
            <div class="modal-footer">
                <?=$form->button('Cancel')->data('dismiss', 'modal')->class('btn btn-default'); ?>
                <?=$form->button('Save')->class('btn btn-primary')->id('save-author'); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->start('scripts'); ?>
<script>
$(function () {
    $("#save-author").on('click', function (e) {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        first_name = first_name.trim();
        last_name = last_name.trim();
        if (first_name.length > 0 && last_name.length > 0) {
            var data = {
                first_name: first_name,
                last_name: last_name
            };
            $.post("<?=$router->pathFor('authors-add'); ?>", data, function (result) {
                if (result.error) {
                    alert(result.message);
                    return;
                }
                
                var author_name = result.author.last_name + ', ' + result.author.first_name;
                $("#author_id").append('<option value="' + result.author.author_id + '">' + author_name + '</option>');
                $("#author_id").val(result.author.author_id);
                alert(result.message);
                
            }, "json");
            $('#new-author').modal('hide');
            return;
        }
        alert('Missing data');
        
        
    });
});
</script>
<?php $this->end(); ?>