<?php $this->layout('layouts::' . $theme) ?>

<h2>Posts</h2>

<div>
    <a href="<?=$router->pathFor('posts-add'); ?>">New Post</a>
</div>

<?php foreach ($posts as $post) : ?>
<h3><?=$this->e($post->title); ?> by <?php echo $post->author->fullname(); ?></h3>
<?=nl2br($post->body); ?>

<p><strong>TAGS:</strong> <?=$post->getTags(); ?></p>
<p><a href="<?=$router->pathFor('posts-edit', ['id'=>$post->post_id]); ?>">Edit</a></p>
<hr/>
<?php endforeach; ?>
