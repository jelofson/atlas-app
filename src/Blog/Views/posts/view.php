<?php $this->layout('layouts::' . $theme) ?>


<?=$this->alerts($messages); ?>

<h2><?=$this->e($post->title); ?> by <?=$this->e($post->author->fullname()); ?></h2>
<p><?=$this->e($post->created); ?></p>

<?=nl2br($this->e($post->body)); ?>

<p><strong>TAGS:</strong> <?=$post->getTags(); ?></p>
<hr/>
<p>
    <a href="<?=$router->pathFor('posts-edit', ['id'=>$post->post_id]); ?>">Edit</a>
    <a href="<?=$router->pathFor('home'); ?>">View all</a>
</p>
