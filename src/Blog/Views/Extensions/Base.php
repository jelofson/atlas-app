<?php
namespace Blog\Views\Extensions;

use League\Plates\Extension\ExtensionInterface;

abstract class Base implements ExtensionInterface {
	public $template;
	protected $engine;
	protected $container;

	
	public function __construct($container)
	{
		$this->container = $container;
	}
}