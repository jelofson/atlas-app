<?php

namespace Blog\Controllers;


class Authors extends Controller {
    
    public function add()
    {
        if (! $this->request->isXhr()) {
            return $this->notFound();
        }
        
        $first_name = filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_STRING);
        $last_name = filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING);
        
        if (! $first_name || ! $last_name) {
            $data = [
                'error'=>true,
                'message'=>'Missing first name or last name'
            ];
            return $this->response->getBody()->write(json_encode($data));
        }
        
        $author = $this->atlas->newRecord(\Blog\DataSource\Authors\AuthorsMapper::class, [
            'first_name'=>$first_name,
            'last_name'=>$last_name
        ]);
        if ($this->atlas->insert($author)) {
            $data = [
                'error'=>false,
                'message'=>'Author inserted successfully',
                'author'=>[
                    'author_id'=>$author->author_id,
                    'first_name'=>$author->first_name,
                    'last_name'=>$author->last_name,
                ]
            ];
            return $this->response->getBody()->write(json_encode($data));
        }
        $data = [
            'error'=>true,
            'message'=>$this->atlas->getException()
        ];
        return $this->response->getBody()->write(json_encode($data));
        
    }
}
