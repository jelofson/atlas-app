<?php

namespace Blog\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Interop\Container\ContainerInterface;

/**
 * Base controller from which to extend
 *
 * @author jon.elofson@gmail.com
 *
 */
abstract class Controller {

    /**
     * The container-interop container interface
     * @var \Interop\Container\ContainerInterface
     */
    protected $container;

    /**
     * The PSR 7 request
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    protected $request;

    /**
     * The PSR 7 response
     * @var \Psr\Http\Message\ResponseInterface
     */
    protected $response;

    /**
     * Messages that can be passed to the view
     *
     * @var array
     */
    protected $messages = [
        'info'=>[],
        'error'=>[]
    ];

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    public function __construct(ContainerInterface $container, ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->container = $container;
        $this->request = $request;
        $this->response = $response;
        
        $this->registerEvents();
        
        // Check auth status
        if ($this->auth->isIdle()) {
            $this->addInfoMessage('You are no logger logged in. Reason: idle');
        } else if ($this->auth->isExpired()) {
            $this->addInfoMessage('You are no logger logged in. Reason: session expired');
        }
        
        $flashInfo = $this->getFlash('info');
        $flashError = $this->getFlash('error');

        $this->addInfoMessage($flashInfo);
        $this->addErrorMessage($flashError);
    }


    /**
     * Magic method to retrive properties of the class, or services in the $services array if they exist
     *
     * @param string $property The property name
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        if ($this->container->has($property)) {
            return $this->container->get($property);
        }

        trigger_error('Undefined property ' . $property . ' in ' . get_class($this));
    }

    /**
     * Set a 404 not found status with an appropriate view template.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function notFound()
    {
        $content = $this->view->render('error', [
            'message'=>'404 not found',
            'messages'=>$this->messages
        ]);
        return $this->response->withStatus(404)->write($content);
    }

    /**
     * Set a 403 not allowed status with an appropriate view template.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function notAllowed()
    {
        $content = $this->view->render('error', [
            'message'=>'403 forbidden',
            'messages'=>$this->messages
        ]);
        $response = $this->response->withStatus(403);
        return $response->write($content);
    }

    /**
     * Set a 400 bad request status with an appropriate view template.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function badRequest($note = null)
    {
        if ($note) {
            $note = " ($note)";
        }
        $content = $this->view->render('error', [
            'message'=>'400 bad request' . $note,
            'messages'=>$this->messages
        ]);
        $response = $this->response->withStatus(400);
        return $response->write($content);
    }

    /**
     * Redirect to a differnet url
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function redirect($url)
    {
        $uri = $this->request->getUri();
        $basePath = $uri->getBasePath();
        $url = ltrim($url, '/');
        return $this->response->withHeader('Location', $basePath . '/' . $url);

    }
    
    protected function registerEvents()
    {
        // Override
    }

    /**
     * Get an aura session flash message by key and segment
     *
     * @param string $key The flash name
     * @param string $segmentName The session segment name. Defaults to the base controller class
     * @return string The flash message
     */
    protected function getFlash($key, $segmentName = null)
    {
        if (! $segmentName) {
            $segmentName = get_parent_class($this);
        }

        $segment = $this->session->getSegment($segmentName);
        return $segment->getFlash($key);

    }

    /**
     * Set a flash message in session
     *
     * @param string $key The flash name
     * @param string $message The flash message
     * @param string $segmentName The session segment. Defaults to the base controller class
     */
    protected function setFlash($key, $message, $segmentName = null)
    {
        if (! $segmentName) {
            $segmentName = get_parent_class($this);
        }

        $segment = $this->session->getSegment($segmentName);
        $segment->setFlash($key, $message);
    }

    /**
     * Add a string message to the `info` key in the messages array
     *
     * @param string $message
     */
    protected function addInfoMessage($message)
    {
        if ($message) {
            $this->messages['info'][] = $message;
        }
    }

    /**
     * Add a string message to the `error` key in the messages array
     *
     * @param string $message
     */
    protected function addErrorMessage($message)
    {
        if ($message) {
            $this->messages['error'][] = $message;
        }
    }
}
