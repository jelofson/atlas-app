<?php

namespace Blog\Controllers;

use Blog\DataSource\Posts\PostsMapper;
use Blog\DataSource\Authors\AuthorsMapper;
use Blog\DataSource\Tags\TagsMapper;
use Blog\DataSource\Taggings\TaggingsMapper;

class Posts extends Controller {

    protected $whitelist = [
        'title',
        'body',
        'author_id',
        'tags'
    ];

    public function index()
    {
        $posts = $this->atlas->select(PostsMapper::class)
            ->orderBy(['created DESC'])
            ->limit(5)
            ->with([
                'author','taggings', 'tags'
            ])
            ->fetchRecordSet();

        $content = $this->view->render('posts/index', [
            'messages'=>$this->messages,
            'posts'=>$posts,
        ]);

        return $this->response->write($content);
    }

    public function view($id)
    {
        $post = $this->atlas->fetchRecord(PostsMapper::class, $id, [
            'author',
            'taggings',
            'tags'
        ]);
        if (! $post) {
            return $this->notFound();
        }

        $content = $this->view->render('posts/view', [
            'messages'=>$this->messages,
            'post'=>$post
        ]);

        return $this->response->getBody()->write($content);

    }

    public function add()
    {
        $post = $this->atlas->newRecord(PostsMapper::class);
        $content = $this->buildForm($post, 'add');

        return $this->response->getBody()->write($content);
    }

    public function doAdd()
    {

        $data = $this->sanitizePost();
        $now = new \DateTime();
        $data['created'] = $now->format('Y-m-d H:i:s');

        $author = $this->atlas->fetchRecord(AuthorsMapper::class, $data['author_id']);
        $data['author'] = $author;
        $post = $this->atlas->newRecord(PostsMapper::class, $data);

        if ($post->validate() && $this->atlas->insert($post)) {
            $this->setFlash('info', 'Post inserted');
            return $this->redirect($this->router->pathFor('posts-view', ['id'=>$post->post_id]));
        }

        foreach ($post->getErrors() as $field=>$message) {
            $this->addErrorMessage($message);
        }

        $content = $this->buildForm($post, 'add');

        return $this->response->getBody()->write($content);
    }

    public function edit($id)
    {
        $post = $this->atlas->fetchRecord(PostsMapper::class, $id, ['taggings', 'tags']);
        $content = $this->buildForm($post, 'edit');

        return $this->response->getBody()->write($content);
    }

    public function doEdit($id)
    {
        $data = $this->sanitizePost();

        $post = $this->atlas->fetchRecord(PostsMapper::class, $id, ['taggings']);
        if (! $post) {
            return $this->notFound();
        }

        $author = $this->atlas->fetchRecord(AuthorsMapper::class, $data['author_id']);

        // instead of looping through the tags and seeing which are new, removed, etc.
        // just delete any if they exist and all as new

        if (! $post->taggings) {
            $post->taggings = $this->atlas->newRecordSet(TaggingsMapper::class);
        }

        foreach ($post->taggings as $tagging) {
            $tagging->markForDeletion();
        }

        foreach ($data['tags'] as $tag) {
            // if the tag exists, use it, else create new
            $tag = trim($tag);
            $tagRecord = $this->atlas->fetchRecordBy(TagsMapper::class, ['tag'=>$tag]);
            if (! $tagRecord) {
                $tagRecord = $this->atlas->newRecord(TagsMapper::class, ['tag'=>$tag]);
            }

            $post->taggings->appendNew([
                'post'=>$post,
                'tag'=>$tagRecord
            ]);

        }


        $post->title = $data['title'];
        $post->body = $data['body'];
        $post->author_id = $data['author_id'];
        
        // Can also assign the author via $post->author = $author


        if ($post->validate() && $this->atlas->persist($post)) {
            $this->setFlash('info', 'Post updated');
            return $this->redirect($this->router->pathFor('posts-view', ['id'=>$post->post_id]));
        }

        foreach ($post->getErrors() as $field=>$message) {
            $this->addErrorMessage($message);
        }

        $content = $this->buildForm($post, 'edit');

        return $this->response->getBody()->write($content);
    }

    protected function buildForm($post, $method)
    {
        $authors = $this->atlas->select(AuthorsMapper::class)
            ->orderBy(['last_name ASC', 'first_name ASC'])
            ->fetchRecordSet();

        if (! $authors) {
            $authors = $this->atlas->newRecordSet(AuthorsMapper::class);
        }
        $content = $this->view->render('posts/form', [
            'messages'=>$this->messages,
            'post'=>$post,
            'form'=>$this->form->autoLf(),
            'authors'=>$authors->asPairs(),
            'method'=>$method
        ]);
        return $content;
    }
    protected function sanitizePost()
    {
        $data = [];
        foreach ($this->whitelist as $key) {
            $data[$key] = trim(filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING));
        }

        // create an array of tags
        $tags = [];
        if ($data['tags']) {
            $tags = $this->cleanUpTags($data['tags']);
        }
        $data['tags'] = $tags;

        return $data;

    }
    
    protected function cleanUpTags($tags)
    {
        $clean = [];
        $tags = explode(',', $tags); 
        foreach ($tags as $tag) {
            $tag = trim($tag);
            if ($tag) {
                $clean[] = $tag;
            }
        }
        return $clean;
    }
    
}
