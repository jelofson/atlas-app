<?php
namespace Blog\DataSource\Taggings;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class TaggingsMapperEvents extends MapperEvents
{
}
