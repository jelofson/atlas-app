<?php
namespace Blog\DataSource\Taggings;

use Atlas\Orm\Mapper\AbstractMapper;
use Blog\DataSource\Posts\PostsMapper;
use Blog\DataSource\Tags\TagsMapper;

/**
 * @inheritdoc
 */
class TaggingsMapper extends AbstractMapper
{
    /**
     * @inheritdoc
     */
    protected function setRelated()
    {
        $this->manyToOne('post', PostsMapper::class);
        $this->manyToOne('tag', TagsMapper::class);
    }
}
