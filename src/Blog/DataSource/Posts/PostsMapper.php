<?php
namespace Blog\DataSource\Posts;

use Atlas\Orm\Mapper\AbstractMapper;
use Blog\DataSource\Authors\AuthorsMapper;
use Blog\DataSource\Tags\TagsMapper;
use Blog\DataSource\Taggings\TaggingsMapper;

/**
 * @inheritdoc
 */
class PostsMapper extends AbstractMapper
{
    /**
     * @inheritdoc
     */
    protected function setRelated()
    {
        $this->manyToOne('author', AuthorsMapper::class);
        $this->oneToMany('taggings', TaggingsMapper::class);
        $this->manyToMany('tags', TagsMapper::class, 'taggings');
    }
}
