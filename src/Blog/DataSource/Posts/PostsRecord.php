<?php
namespace Blog\DataSource\Posts;

use Atlas\Orm\Mapper\Record;

/**
 * @inheritdoc
 */
class PostsRecord extends Record
{
    protected $errors = [];

    public function getTags($separator = ', ')
    {
        if (! $this->tags) {
            return;
        }
        $tags = [];
        foreach ($this->tags as $tag) {
            $tags[] = $tag->tag;
        }

        return implode($separator, $tags);
    }

    public function validate()
    {
        $required = [
            'title'=>'Post title',
            'body'=>'Post body',
            'author_id'=>'Author'
        ];
        foreach ($required as $field=>$label) {
            if (! $this->$field) {
                $this->errors[$field] = $label . ' can not be empty';
            }
        }

        return count($this->errors) == 0;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
