<?php
namespace Blog\DataSource\Tags;

use Atlas\Orm\Mapper\AbstractMapper;
use Blog\DataSource\Taggings\TaggingsMapper;
use Blog\DataSource\Tags\TagsMapper;

/**
 * @inheritdoc
 */
class TagsMapper extends AbstractMapper
{
    /**
     * @inheritdoc
     */
    protected function setRelated()
    {
        $this->oneToMany('taggings', TaggingsMapper::class);
        $this->manyToMany('tags', TagsMapper::class, 'taggings');
    }
}
