<?php
namespace Blog\DataSource\Tags;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class TagsMapperEvents extends MapperEvents
{
}
