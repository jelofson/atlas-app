<?php
namespace Blog\DataSource\Authors;

use Atlas\Orm\Mapper\AbstractMapper;

/**
 * @inheritdoc
 */
class AuthorsMapper extends AbstractMapper
{
    /**
     * @inheritdoc
     */
    protected function setRelated()
    {
        // no related fields
    }
}
