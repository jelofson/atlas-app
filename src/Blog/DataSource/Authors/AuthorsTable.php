<?php
/**
 * This table class was generated by Atlas. Changes will be overwritten.
 */
namespace Blog\DataSource\Authors;

use Atlas\Orm\Table\AbstractTable;

/**
 * @inheritdoc
 */
class AuthorsTable extends AbstractTable
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function getColNames()
    {
        return [
            'author_id',
            'first_name',
            'last_name',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCols()
    {
        return [
            'author_id' => (object) [
                'name' => 'author_id',
                'type' => 'integer',
                'size' => null,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => true,
                'primary' => true,
            ],
            'first_name' => (object) [
                'name' => 'first_name',
                'type' => 'varchar',
                'size' => 50,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
            'last_name' => (object) [
                'name' => 'last_name',
                'type' => 'varchar',
                'size' => 50,
                'scale' => null,
                'notnull' => true,
                'default' => null,
                'autoinc' => false,
                'primary' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKey()
    {
        return [
            'author_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAutoinc()
    {
        return 'author_id';
    }

    /**
     * @inheritdoc
     */
    public function getColDefaults()
    {
        return [
            'author_id' => null,
            'first_name' => null,
            'last_name' => null,
        ];
    }
}
