<?php
namespace Blog\DataSource\Authors;

use Atlas\Orm\Mapper\RecordSet;

/**
 * @inheritdoc
 */
class AuthorsRecordSet extends RecordSet
{
    public function asPairs()
    {
        $pairs = [''=>'----------'];
        foreach ($this as $record) {
            $pairs[$record->author_id] = $record->fullname('lastfirst');
        }
        return $pairs;
    }
}
