<?php
namespace Blog\DataSource\Authors;

use Atlas\Orm\Mapper\Record;

/**
 * @inheritdoc
 */
class AuthorsRecord extends Record
{
    public function fullname($order = 'firstlast')
    {
        switch ($order) {
            case 'firstlast':
                return $this->first_name . ' ' . $this->last_name;
                break;
            case 'lastfirst':
                return $this->last_name . ', ' . $this->first_name;
            default:
                return $this->first_name . ' ' . $this->last_name;
                break;
        }
    }
}
