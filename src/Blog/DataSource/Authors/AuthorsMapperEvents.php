<?php
namespace Blog\DataSource\Authors;

use Atlas\Orm\Mapper\MapperEvents;
use Atlas\Orm\Mapper\MapperInterface;
use Atlas\Orm\Mapper\RecordInterface;

/**
 * @inheritdoc
 */
class AuthorsMapperEvents extends MapperEvents
{
}
