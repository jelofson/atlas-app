<?php
namespace Cli;

use League\CLImate\CLImate;

class Controller extends Base {

    protected $extends;
    protected $climate;

    public function __construct($base, $project, $controller, $theme, $extends = 'Controller')
    {
        $this->extends = ucfirst($extends);
        $this->climate = new CLImate();
        parent::__construct($base, $project, $controller, $theme);
    }
    
    public function build()
    {
        if ($this->checkControllerPath()) {
            $this->info('Project exists with controller folder');
        } else {
            $this->error('The controller path does not exist. You must have a ' . $this->project . '/Controllers folder');
            return;
        }
        
        if ($this->controllerExists()) {
            $overwrite = $this->climate->confirm('Controller file already exists. Do you want to overwrite?');
            if ($overwrite->confirmed()) {
                $this->info('Overwriting existing controller');
            } else {
                $this->info('Not overwriting existing file. Done.');
                return;
            }
        }
        
        if ($this->createController()) {
            $this->info('Controller file created');
        } else {
            $this->error('Controller file NOT created ' . $this->getWarning());
        }
        
        if ($this->createControllerViewFolder()) {
            $this->info('Controller view folder file created');
        } else {
            $this->error('Controller view folder NOT created ' . $this->getWarning());
        }
        
        if ($this->createControllerIndexView()) {
            $this->info('Controller index view file created');
        } else {
            $this->error('Controller index view file NOT created ' . $this->getWarning());
        }
        
        $this->build_status = self::BUILD_STATUS_OK;
    }
    
    protected function checkControllerPath()
    {
        $controller_path = $this->base_path . '/src/' . $this->project . '/Controllers';
        return file_exists($controller_path);
    }
    
    protected function controllerExists()
    {
        $filename = $this->base_path . '/src/' . $this->project . '/Controllers/' . $this->controller . '.php';
        return file_exists($filename);
    }
    
    protected function createController()
    {
        
        if ($this->extends) {
            $extends = 'extends ' . $this->extends;
        }
        $template = file_get_contents($this->base_path . '/script/templates/controller.php');
		$template = str_replace('{project}', $this->project, $template);
		$template = str_replace('{class}', $this->controller, $template);
		$template = str_replace('{extends}', $extends, $template);
        $template = str_replace('{folder}', strtolower($this->controller), $template);
		return file_put_contents($this->base_path . '/src/'. $this->project . '/Controllers/' . $this->controller . '.php' , $template);
    }
    
    protected function createControllerViewFolder()
    {
        $view_folder = strtolower($this->controller);
		return mkdir($this->base_path . '/src/' . $this->project . '/Views/' . $view_folder);
		
    }
    
    protected function createControllerIndexView()
    {
        
        $view_folder = strtolower($this->controller);
        return copy($this->base_path . '/script/templates/views/index.php', $this->base_path . '/src/' . $this->project . '/Views/' . $view_folder . '/index.php');
    }
    
    protected function setWarning($errno, $errstr)
	{
		$this->warning = $errstr;
	}
    
    protected function getWarning()
    {
        $warning = $this->warning;
        $this->warning = null;
        return $warning;
    }
    
}
