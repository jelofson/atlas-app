<?php
namespace Cli;

class Commands {
    public static function postCreateProject()
    {
        copy('./html/index.php.dist', './html/index.php'); 
        copy('./html/htaccess.dist', './html/.htaccess');
        unlink('README.md');
    }
}