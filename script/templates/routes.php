<?php
// default route
$app->get('/', function ($request, $response) {
    $controller = new \{project}\Controllers\Index($this, $request, $response);
    return $controller->index();
});

// Define project routes
// These are just example routes
$app->group('/login', function() {
    $this->get('', function ($request, $response) {
        // Replace "Project" with your project namespace
        $controller = new \{project}\Controllers\Index($this, $request, $response);
        
        return $controller->login();
    });
    $this->post('', function ($request, $response) {
        // Replace "Project" with your project namespace
        $controller = new \{project}\Controllers\Index($this, $request, $response);
        
        return $controller->doLogin();
    });
});
$app->get('/logout', function ($request, $response) {
    // Replace "Project" with your project namespace
    $controller = new \{project}\Controllers\Index($this, $request, $response);
    
    return $controller->logout();
});

