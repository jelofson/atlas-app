<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie9" lang="en" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" dir="ltr">
<!--<![endif]-->
    <?php $this->insert('layouts::gcweb/head'); ?>
    
    <body vocab="http://schema.org/" typeof="WebPage">
        <!-- Google Tag Manager DO NOT REMOVE OR MODIFY - NE PAS SUPPRIMER OU MODIFIER -->
        <noscript><iframe title="Google Tag Manager" src="//www.googletagmanager.com/ns.html?id=GTM-TLGQ9K" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer1'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer1','GTM-TLGQ9K');</script>
        <!-- End Google Tag Manager -->
        <ul id="wb-tphp">
            <li class="wb-slc">
                <a class="wb-sl" href="#wb-cont">Skip to main content</a>
            </li>
            <li class="wb-slc visible-sm visible-md visible-lg">
                <a class="wb-sl" href="#wb-info">Skip to "About this site"</a>
            </li>
        </ul>
        <?php $this->insert('layouts::gcweb/header'); ?>
        
        <main role="main" property="mainContentOfPage" class="container">
            <h1>Government of Canada Layout</h1>
            <?=$this->section('content')?>
        </main>

        <?php $this->insert('layouts::gcweb/footer'); ?>
        
        <!--[if gte IE 9 | !IE ]><!-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
        <script src="<?=$baseUri; ?>/gc/wet-boew/js/wet-boew.min.js"></script>
        <script src="<?=$baseUri; ?>/gc/wet-boew/js/alert.js"></script>
        <!--<![endif]-->
        <!--[if lt IE 9]>
        <script src="<?=$baseUri; ?>/gc/wet-boew/js/ie8-wet-boew2.min.js"></script>

        <![endif]-->
    </body>
</html>
