        <header role="banner">
            <div id="wb-bnr">
                <div id="wb-bar">
                    <div class="container">
                        <div class="row">
                            <object id="gcwu-sig" type="image/svg+xml" tabindex="-1" role="img" data="<?=$baseUri; ?>/gc/theme-gcwu-fegc/assets/sig-<?php echo $locale->gettext('TEXT_LANG'); ?>.svg" aria-label="<?php echo $locale->gettext('TEXT_GOC'); ?>"></object>
                            <ul id="gc-bar" class="list-inline">
                                <li><a href="<?php echo $locale->gettext('URL_CANADA_CA'); ?>" rel="external">Canada.ca</a></li>
                                <li><a href="<?php echo $locale->gettext('URL_SERVICES'); ?>" rel="external"><?php echo $locale->gettext('TEXT_SERVICES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_DEPARTMENTS'); ?>" rel="external"><?php echo $locale->gettext('TEXT_DEPARTMENTS'); ?></a></li>
                                <li id="wb-lng"><h2><?php echo $locale->gettext('TEXT_LANG_SELECTION'); ?></h2>
                                    <ul class="list-inline">
                                        <li><a lang="<?php echo $locale->gettext('TEXT_ALT_LANG'); ?>" href="content-fr.html"><?php echo $locale->gettext('TEXT_ALT_LANG_FULL'); ?></a></li>
                                    </ul>
                                </li>
                            </ul>
                            <section class="wb-mb-links col-xs-12 visible-sm visible-xs" id="wb-glb-mn">
                                <h2><?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?></h2>
                                <ul class="pnl-btn list-inline text-right">
                                    <li><a href="#mb-pnl" title="<?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?>" aria-controls="mb-pnl" class="overlay-lnk btn btn-sm btn-default" role="button"><span class="glyphicon glyphicon-search"><span class="glyphicon glyphicon-th-list"><span class="wb-inv"><?php echo $locale->gettext('TEXT_SEARCH_MENUS'); ?></span></span></span></a></li>
                                </ul>
                                <div id="mb-pnl"></div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div id="wb-sttl" class="col-md-5">
                            <a href="http://wet-boew.github.io/v4.0-ci/index-en.html">
                            <span><?php echo $locale->gettext('TEXT_NRCAN_FULL'); ?></span>
                            </a>
                        </div>
                        <object id="wmms" type="image/svg+xml" tabindex="-1" role="img" data="<?=$baseUri; ?>/gc/theme-gcwu-fegc/assets/wmms.svg" aria-label="<?php echo $locale->gettext('TEXT_WORDMARK'); ?>"></object>
                        <section id="wb-srch" class="visible-md visible-lg">
                            <h2>Search</h2>
                            <form action="https://google.ca/search" method="get" role="search" class="form-inline">
                                <div class="form-group">
                                    <label for="wb-srch-q"><?php echo $locale->gettext('TEXT_SEARCH_WEBSITE'); ?></label>
                                    <input id="wb-srch-q" class="form-control" name="q" type="search" value="" size="27" maxlength="150">
                                    <input type="hidden" name="q" value="site:wet-boew.github.io OR site:github.com/wet-boew/">
                                </div>
                                <button type="submit" id="wb-srch-sub" class="btn btn-default"><?php echo $locale->gettext('TEXT_SEARCH'); ?></button>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
            <nav role="navigation" id="wb-sm" data-ajax-replace="//www2.nrcan-rncan.gc.ca/cdn/wet4/ajax/menu-include-<?php echo $locale->gettext('TEXT_DCLANG'); ?>.html" data-trgt="mb-pnl" class="wb-menu visible-md visible-lg" typeof="SiteNavigationElement">
                <div class="container nvbar">
                    <h2><?php echo $locale->gettext('TEXT_TOPICS_MENU'); ?></h2>
                    <div class="row">
                        <ul class="list-inline menu">
                            <li><a href="<?php echo $locale->gettext('URL_ENERGY'); ?>"><?php echo $locale->gettext('TEXT_ENERGY'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_MINING_MATERIALS'); ?>"><?php echo $locale->gettext('TEXT_MINING_MATERIALS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_FORESTS'); ?>"><?php echo $locale->gettext('TEXT_FORESTS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_EARTH_SCIENCES'); ?>"><?php echo $locale->gettext('TEXT_EARTH_SCIENCES'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_HAZARDS'); ?>"><?php echo $locale->gettext('TEXT_HAZARDS'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_EXPLOSIVES'); ?>"><?php echo $locale->gettext('TEXT_EXPLOSIVES'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_THE_NORTH'); ?>"><?php echo $locale->gettext('TEXT_THE_NORTH'); ?></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_ENVIRONMENT'); ?>"><?php echo $locale->gettext('TEXT_ENVIRONMENT'); ?></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav role="navigation" id="wb-bc" property="breadcrumb">
                <h2><?php echo $locale->gettext('TEXT_YOU_ARE_HERE'); ?></h2>
                <div class="container">
                    <div class="row">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo $locale->gettext('URL_NRCAN'); ?>"><?php echo $locale->gettext('TEXT_HOME'); ?></a>
                            </li>
                            <li>
                                <a href="http://wet-boew.github.io/v4.0-ci/demos/index-en.html">Working examples</a>
                            </li>
                            <li>
                                <a href="index-en.html">GCWU theme</a>
                            </li>
                            <li>Content page</li>
                        </ol>
                    </div>
                </div>
            </nav>
        </header>