<?php

namespace {project}\Controllers;


class {class} {extends} {
    
    public function index()
    {
        $content = $this->view->render('{folder}/index', []);
        return $this->response->write($content);
    }
}
